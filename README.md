## Getting Started

Create a .env.local file with the following variable:

```bash
API_KEY={YOUR_API_KEY}
```

Using npm
Install the main project dependencies:

```bash
nvm use
npm install
```

Run the development server:

```bash
npm run build
npm run dev
```

## TODO

- Refactor Modal component -  Separate Cast, Genres and Directors into individual components
- Didn't get time to research how to test the state within components when using Context API...I could have done prop drilling to get around this issue, but felt this wouldn't be the correct approach.
- On initial load I would like to have displayed the popular movies through doing a server fetch request using getStaticProps in next.js
- Fix suppressed TS issues
- Update favicon
- Add loading animation when fetching movies

## Retro

- Use scss modules over styled components
- Test unhappy path
