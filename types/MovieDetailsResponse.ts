export type MovieDetailsResponse = {
  cast: Array<{ name: string }>;
  crew: Array<{ name: string }>;
  genres: Array<{ id: number; name: string }>;
  id: number;
  poster_path: string | null;
  overview: string;
  release_date: string;
  title: string;
};
