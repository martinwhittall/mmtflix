export type MoviesResponse = {
  page: number;
  total_results: number;
  total_pages: number;
  results: Array<{
    id: number;
    poster_path: string | null;
    overview: string;
    release_date: string;
    title: string;
  }>;
};
