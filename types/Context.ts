import { Movie } from "./Movie";

export type Context = {
  movieDetails: {};
  setMovieDetails: (movieDetails: {}) => void;
  movieId: number;
  setMovieId: (movies: number) => void;
  movies: Array<Movie>;
  setMovies: (movies: Array<Movie>) => void;
  page: number;
  setPage: (page: number) => void;
  results: number;
  setResults: (results: number) => void;
  showModal: boolean;
  setShowModal: (showModal: boolean) => void;
  search: string;
  setSearch: (search: string) => void;
  totalPages: number;
  setTotalPages: (totalPages: number) => void;
};
