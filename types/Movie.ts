export type Movie = {
  className?: string;
  id: number;
  image: string | null;
  year: string;
  title: string;
};
