export type MovieDetails = {
  cast: Array<string>;
  directors: Array<string>;
  image: string | null;
  genres: Array<string>;
  plot: string;
  title: string;
  year: number;
};
