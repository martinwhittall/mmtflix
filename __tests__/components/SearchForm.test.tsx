import { act, fireEvent, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import SearchForm from "../../components/SearchForm";
import { MoviesProvider } from "../../context/MoviesContext";
import { ThemeProvider } from "styled-components";
import { theme } from "../../styles/theme";

describe("<SearchForm />", () => {
  test("should render search form correctly", () => {
    const { asFragment } = render(
      <MoviesProvider>
        <ThemeProvider theme={theme}>
          <SearchForm />
        </ThemeProvider>
      </MoviesProvider>
    );

    expect(asFragment()).toMatchSnapshot();
  });

  test("should render input", () => {
    render(
      <MoviesProvider>
        <ThemeProvider theme={theme}>
          <SearchForm />
        </ThemeProvider>
      </MoviesProvider>
    );

    const input = screen.getByTestId("search-input");

    expect(input).toBeInTheDocument();
    expect(input).toHaveValue("");
    expect(screen.getByPlaceholderText("Search here...")).toBeInTheDocument();
  });

  test("should render button", () => {
    render(
      <MoviesProvider>
        <ThemeProvider theme={theme}>
          <SearchForm />
        </ThemeProvider>
      </MoviesProvider>
    );

    expect(screen.getByRole("button")).toBeInTheDocument();
    expect(screen.getByRole("img")).toHaveAttribute("alt", "Search");
  });

  test("should allow user to change input value", () => {
    render(
      <MoviesProvider>
        <ThemeProvider theme={theme}>
          <SearchForm />
        </ThemeProvider>
      </MoviesProvider>
    );

    const input = screen.getByTestId("search-input");

    expect(input).toBeInTheDocument();
    expect(input).toHaveValue("");

    userEvent.type(input, "Avengers");

    expect(input).toHaveValue("Avengers");
  });
});
