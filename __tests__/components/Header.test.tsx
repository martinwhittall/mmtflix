import { render, screen } from "@testing-library/react";
import Header from "../../components/Header";
import { MoviesProvider } from "../../context/MoviesContext";
import { ThemeProvider } from "styled-components";
import { theme } from "../../styles/theme";

describe("<Header />", () => {
  test("should render correctly", () => {
    const { asFragment } = render(
      <MoviesProvider>
        <ThemeProvider theme={theme}>
          <Header />
        </ThemeProvider>
      </MoviesProvider>
    );
    expect(asFragment()).toMatchSnapshot();
  });

  test("should render logo with alt tag", () => {
    render(
      <MoviesProvider>
        <ThemeProvider theme={theme}>
          <Header />
        </ThemeProvider>
      </MoviesProvider>
    );

    expect(screen.getByAltText("MMTFlix Logo")).toBeInTheDocument();
  });

  test("should render search", () => {
    render(
      <MoviesProvider>
        <ThemeProvider theme={theme}>
          <Header />
        </ThemeProvider>
      </MoviesProvider>
    );

    const input = screen.getByTestId("search-input");

    expect(input).toBeInTheDocument();
    expect(input).toHaveValue("");
    expect(screen.getByPlaceholderText("Search here...")).toBeInTheDocument();
    expect(screen.getByRole("button")).toBeInTheDocument();
  });
});
