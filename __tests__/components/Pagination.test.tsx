import React from "react";
import { render } from "@testing-library/react";
import Pagination from "../../components/Pagination";
import { MoviesProvider } from "../../context/MoviesContext";
import { ThemeProvider } from "styled-components";
import { theme } from "../../styles/theme";

describe("<Pagination />", () => {
  test("should render correctly", () => {
    const { asFragment } = render(
      <MoviesProvider>
        <ThemeProvider theme={theme}>
          <Pagination />
        </ThemeProvider>
      </MoviesProvider>
    );

    expect(asFragment()).toMatchSnapshot();
  });
});
