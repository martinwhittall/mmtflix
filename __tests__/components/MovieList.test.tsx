import React from "react";
import { act, render, screen } from "@testing-library/react";
import MovieList from "../../components/MovieList";
import { MoviesProvider } from "../../context/MoviesContext";
import { ThemeProvider } from "styled-components";
import { theme } from "../../styles/theme";
import SearchForm from "../../components/SearchForm";

beforeEach(() => {
  // @ts-ignore
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () =>
        Promise.resolve([
          {
            adult: false,
            backdrop_path: "/7RyHsO4yDXtBv1zUU3mTpHeQ0d5.jpg",
            genre_ids: [12, 878, 28],
            id: 299534,
            original_language: "en",
            original_title: "Avengers: Endgame",
            overview:
              "After the devastating events of Avengers: Infinity War, the universe is in ruins due to the efforts of the Mad Titan, Thanos. With the help of remaining allies, the Avengers must assemble once more in order to undo Thanos' actions and restore order to the universe once and for all, no matter what consequences may be in store.",
            popularity: 288.723,
            poster_path: "/or06FN3Dka5tukK1e9sl16pB3iy.jpg",
            release_date: "2019-04-24",
            title: "Avengers: Endgame",
            video: false,
            vote_average: 8.3,
            vote_count: 19659,
          },
          {
            adult: false,
            backdrop_path: "/nNmJRkg8wWnRmzQDe2FwKbPIsJV.jpg",
            genre_ids: [878, 28, 12],
            id: 24428,
            original_language: "en",
            original_title: "The Avengers",
            overview:
              "When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!",
            popularity: 248.231,
            poster_path: "/RYMX2wcKCBAr24UyPD7xwmjaTn.jpg",
            release_date: "2012-04-25",
            title: "The Avengers",
            video: false,
            vote_average: 7.7,
            vote_count: 25813,
          },
          {
            adult: false,
            backdrop_path: "/lmZFxXgJE3vgrciwuDib0N8CfQo.jpg",
            genre_ids: [12, 28, 878],
            id: 299536,
            original_language: "en",
            original_title: "Avengers: Infinity War",
            overview:
              "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.",
            popularity: 378.065,
            poster_path: "/7WsyChQLEftFiDOVTGkv3hFpyyt.jpg",
            release_date: "2018-04-25",
            title: "Avengers: Infinity War",
            video: false,
            vote_average: 8.3,
            vote_count: 23237,
          },
          {
            adult: false,
            backdrop_path: "/xnqust9Li4oxfhXD5kcPi3UC8i4.jpg",
            genre_ids: [28, 12, 878],
            id: 99861,
            original_language: "en",
            original_title: "Avengers: Age of Ultron",
            overview:
              "When Tony Stark tries to jumpstart a dormant peacekeeping program, things go awry and Earth’s Mightiest Heroes are put to the ultimate test as the fate of the planet hangs in the balance. As the villainous Ultron emerges, it is up to The Avengers to stop him from enacting his terrible plans, and soon uneasy alliances and unexpected action pave the way for an epic and unique global adventure.",
            popularity: 155.29,
            poster_path: "/4ssDuvEDkSArWEdyBl2X5EHvYKU.jpg",
            release_date: "2015-04-22",
            title: "Avengers: Age of Ultron",
            video: false,
            vote_average: 7.3,
            vote_count: 18876,
          },
          {
            adult: false,
            backdrop_path: "/qzzAt0GakCZzbCeCJ0sFGhMIcdv.jpg",
            genre_ids: [28, 14],
            id: 323660,
            original_language: "en",
            original_title: "Avengers Grimm",
            overview:
              'When Rumpelstiltskin destroys the Magic Mirror and escapes to the modern world, the four princesses of "Once Upon a Time"-Cinderella, Sleeping Beauty, Snow White, and Rapunzel-are sucked through the portal too. Well-trained and endowed with magical powers, the four women must fight Rumpelstiltskin and his army of thralls before he enslaves everyone on Earth.',
            popularity: 47.57,
            poster_path: "/1SbBKCbnULACOqWKN7eLfTu1gVm.jpg",
            release_date: "2015-03-17",
            title: "Avengers Grimm",
            video: false,
            vote_average: 4.3,
            vote_count: 85,
          },
        ]),
    })
  );
});

describe("<MovieList />", () => {
  test("should render correctly", async () => {
    const { asFragment } = render(
      <MoviesProvider>
        <ThemeProvider theme={theme}>
          <MovieList />
        </ThemeProvider>
      </MoviesProvider>
    );

    render(
      <MoviesProvider>
        <ThemeProvider theme={theme}>
          <SearchForm />
        </ThemeProvider>
      </MoviesProvider>
    );

    await act(() => Promise.resolve());

    expect(asFragment()).toMatchSnapshot();
  });

  // test("should render list of movies", async () => {
  //   await act(async () => render(<MovieList />));
  //
  //   await act(() => Promise.resolve());
  //
  //   expect(screen.getAllByRole("img")).toHaveLength(5);
  // });
});
