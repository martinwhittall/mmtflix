import React from "react";
import { render, screen } from "@testing-library/react";
import MovieItem from "../../components/MovieItem";
import { ThemeProvider } from "styled-components";
import { theme } from "../../styles/theme";

const props = {
  id: 24428,
  image: "https://image.tmdb.org/t/p/w500//or06FN3Dka5tukK1e9sl16pB3iy.jpg",
  year: "2019",
  title: "Avengers: Endgame",
};

describe("<MovieItem />", () => {
  test("should render correctly", () => {
    const { asFragment } = render(
      <ThemeProvider theme={theme}>
        <MovieItem {...props} />
      </ThemeProvider>
    );

    expect(asFragment()).toMatchSnapshot();
  });

  test("should render movie title", () => {
    render(
      <ThemeProvider theme={theme}>
        <MovieItem {...props} />
      </ThemeProvider>
    );

    expect(screen.getByText(/Avengers: Endgame/)).toBeInTheDocument();
  });

  test("should render optimised movie image with alt tag", () => {
    render(
      <ThemeProvider theme={theme}>
        <MovieItem {...props} />
      </ThemeProvider>
    );

    const image = screen.getByRole("img");
    expect(image).toHaveAttribute(
      "src",
      "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
    );
    expect(image).toHaveAttribute("alt", "Avengers: Endgame poster");
  });

  test("should render movie release date", () => {
    render(
      <ThemeProvider theme={theme}>
        <MovieItem {...props} />
      </ThemeProvider>
    );

    expect(screen.getByText(/2019/)).toBeInTheDocument();
  });
});
