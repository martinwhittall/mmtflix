import { theme } from "../../styles/theme";
import { getDirector } from "../../utils/getDirector";

describe("getDirector", () => {
  const crew = [
    {
      adult: false,
      gender: 0,
      id: 1413119,
      known_for_department: "Production",
      name: "Michael-Ryan Fletchall",
      original_name: "Michael-Ryan Fletchall",
      credit_id: "5ca6bfb6c3a3683b28aa95a5",
      department: "Camera",
      job: "Aerial Camera",
    },
    {
      known_for_department: "Visual Effects",
      name: "Matt Aitken",
      department: "Visual Effects",
      job: "Visual Effects Supervisor",
    },
    {
      known_for_department: "Directing",
      name: "Anthony Russo",
      department: "Directing",
      job: "Director",
    },
    {
      known_for_department: "Costume & Make-Up",
      name: "Janine Rath",
      department: "Costume & Make-Up",
      job: "Hair Department Head",
    },
    {
      known_for_department: "Visual Effects",
      name: "Jerad Marantz",
      department: "Art",
      job: "Concept Artist",
    },
  ];

  test("should get the director from the crew", () => {
    expect(getDirector(crew)).toEqual(["Anthony Russo"]);
  });
});
