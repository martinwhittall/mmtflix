import { getYear } from "../../utils/getYear";

describe("getYear", () => {
  test("should get the year from the date", () => {
    expect(getYear("2012-04-25'")).toEqual(2012);
  });
});
