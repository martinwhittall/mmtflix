import { theme } from "../../styles/theme";

describe("theme", () => {
  test("should render the theme correctly", () => {
    expect(theme).toEqual({
      backgroundColors: {
        backgroundColor1: "#1d1d1d",
        backgroundColor2: "#2c2c2c",
        backgroundColor3: "#ffffff",
        backgroundColor4: "#a9a9a9",
      },
      borderColors: {
        borderColor1: "#ffffff",
        borderColor2: "#a9a9a9",
      },
      breakpoints: {
        lg: "896px",
        md: "768px",
        sm: "640px",
        xl: "1080px",
        xl2: "1600px",
        xs: "480px",
      },
      fontColors: {
        fontColor1: "#1d1d1d",
        fontColor2: "#ffffff",
      },
      fontSizes: {
        lg: "1.125rem",
        md: "1rem",
        xl: "1.25rem",
        xl2: "2.5rem",
      },
      palette: {
        color1: "#1d1d1d",
        color2: "#2c2c2c",
        color3: "#ffffff",
        color4: "#a9a9a9",
      },
      spacing: {
        lg: "1.125rem",
        md: "1rem",
        sm: "0.938rem",
        xl: "1.25rem",
        xl2: "1.875rem",
        xl3: "2.5rem",
        xl4: "3.75rem",
        xs: "0.625rem",
        xs2: "0.313rem",
        xs3: "0.25rem",
      },
    });
  });
});
