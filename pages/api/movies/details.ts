// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { movieDetailsMapper } from "../../../utils/movieDetailsMapper";
import { moviesMapper } from "../../../utils/moviesMapper";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { id } = req.query;

  const movieDetailsResults = await fetch(
    `https://api.themoviedb.org/3/movie/${id}?api_key=${process.env.API_KEY}&language=en-US`
  );
  const movieDetailsData = await movieDetailsResults.json();

  const movieCreditsResults = await fetch(
    `https://api.themoviedb.org/3/movie/${id}/credits?api_key=${process.env.API_KEY}&language=en-US`
  );
  const movieCreditsData = await movieCreditsResults.json();

  res
    .status(200)
    .json(movieDetailsMapper({ ...movieDetailsData, ...movieCreditsData }));
}
