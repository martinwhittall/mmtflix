// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { moviesMapper } from "../../../utils/moviesMapper";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { search, page = 1 } = req.query;

  const result = await fetch(
    `https://api.themoviedb.org/3/search/movie?api_key=${process.env.API_KEY}&language=en-US&query=${search}&page=${page}&include_adult=false`
  );

  const data = await result.json();

  res.status(200).json(moviesMapper(data));
}
