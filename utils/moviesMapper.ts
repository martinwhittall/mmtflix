import { getYear } from "./getYear";
import { MoviesResponse } from "../types/MoviesResponse";

const moviesMapper = ({
  page,
  results = [],
  total_results,
  total_pages,
}: MoviesResponse) => ({
  movies: results.map(({ id, title, poster_path, release_date }) => ({
    id,
    title,
    image: poster_path
      ? `https://image.tmdb.org/t/p/w500/${poster_path}`
      : null,
    year: getYear(release_date),
  })),
  page,
  totalResults: total_results,
  totalPages: total_pages,
});

export { moviesMapper };
