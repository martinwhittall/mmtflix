const getYear = (date: string) => new Date(date).getFullYear();

export { getYear };
