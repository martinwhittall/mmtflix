type Crew = Array<{
  job?: string;
  name: string;
}>;

const getDirector = (crew: Crew) =>
  crew.filter((member) => member.job === "Director").map(({ name }) => name);

export { getDirector };
