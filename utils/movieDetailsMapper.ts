import { getYear } from "./getYear";
import { MovieDetailsResponse } from "../types/MovieDetailsResponse";
import { getDirector } from "./getDirector";
import { MovieDetails } from "../types/MoviesDetails";

const movieDetailsMapper = ({
  cast = [],
  crew = [],
  genres = [],
  overview,
  poster_path,
  release_date,
  title,
}: MovieDetailsResponse): MovieDetails => ({
  cast: cast.map(({ name }) => name).slice(0, 5),
  directors: getDirector(crew),
  image: poster_path ? `https://image.tmdb.org/t/p/w500/${poster_path}` : null,
  genres: genres.map(({ name }) => name),
  plot: overview,
  title,
  year: getYear(release_date),
});

export { movieDetailsMapper };
