import {
  backgroundColors,
  BackgroundColors,
  borderColors,
  BorderColors,
  fontColors,
  FontColors,
  palette,
  Palette,
} from "./colors";
import { FontSizes, fontSizes } from "./fontSizes";
import { Spacing, spacing } from "./spacing";
import { Breakpoints, breakpoints } from "./breakpoints";

type Theme = {
  palette: Palette;
  backgroundColors: BackgroundColors;
  borderColors: BorderColors;
  fontColors: FontColors;
  spacing: Spacing;
  breakpoints: Breakpoints;
  fontSizes: FontSizes;
};

const theme: Theme = {
  palette,
  backgroundColors,
  borderColors,
  fontColors,
  fontSizes,
  spacing,
  breakpoints,
};

export { theme };
export type { Theme };
