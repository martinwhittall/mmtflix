type Spacing = {
  xs3: string;
  xs2: string;
  xs: string;
  sm: string;
  md: string;
  lg: string;
  xl: string;
  xl2: string;
  xl3: string;
  xl4: string;
};

const spacing: Spacing = {
  /** 4px */
  xs3: "0.25rem",
  /** 5px */
  xs2: "0.313rem",
  /** 10px */
  xs: "0.625rem",
  /** 15px */
  sm: "0.938rem",
  /** 16px */
  md: "1rem",
  /** 18px */
  lg: "1.125rem",
  /** 20px */
  xl: "1.25rem",
  /** 30px */
  xl2: "1.875rem",
  /** 40px */
  xl3: "2.5rem",
  /** 60px */
  xl4: "3.75rem",
};

export { spacing };
export type { Spacing };
