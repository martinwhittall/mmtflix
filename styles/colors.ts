type Palette = {
  color1: string;
  color2: string;
  color3: string;
  color4: string;
};

type BackgroundColors = {
  backgroundColor1: string;
  backgroundColor2: string;
  backgroundColor3: string;
  backgroundColor4: string;
};

type BorderColors = {
  borderColor1: string;
  borderColor2: string;
};

type FontColors = {
  fontColor1: string;
  fontColor2: string;
};

const palette: Palette = {
  color1: "#1d1d1d",
  color2: "#2c2c2c",
  color3: "#ffffff",
  color4: "#a9a9a9",
};

const backgroundColors: BackgroundColors = {
  backgroundColor1: palette.color1,
  backgroundColor2: palette.color2,
  backgroundColor3: palette.color3,
  backgroundColor4: palette.color4,
};

const borderColors: BorderColors = {
  borderColor1: palette.color3,
  borderColor2: palette.color4,
};

const fontColors: FontColors = {
  fontColor1: palette.color1,
  fontColor2: palette.color3,
};

export { palette, backgroundColors, borderColors, fontColors };
export type { Palette, BackgroundColors, BorderColors, FontColors };
