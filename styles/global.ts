import { theme } from "./theme";
import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-display: swap;
    src: local('Roboto'), url(/fonts/Roboto/Roboto-Regular.ttf) format('truetype');
  }

  @font-face {
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 500;
    font-display: swap;
    src: local('Roboto'), url(/fonts/Roboto/Roboto-Medium.ttf) format('truetype');
  }

  @font-face {
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 700;
    font-display: swap;
    src: local('Roboto'), url(/fonts/Roboto/Roboto-Bold.ttf) format('truetype');
  }
  
  html {
    box-sizing: border-box;
    font-size: 16px;
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }

  body, h1, h2, h3, h4, h5, h6, p, ol, ul {
    margin: 0;
    padding: 0;
    font-weight: normal;
  }

  ol, ul {
    list-style: none;
  }

  img {
    max-width: 100%;
    height: auto;
  }

  body {
    background-color: ${theme.backgroundColors.backgroundColor1};
    color: ${theme.fontColors.fontColor2};
    font-family: 'Roboto', sans-serif;
    font-size: ${theme.fontSizes.md};
    font-weight: 500;
    line-height: 24px;
    margin: 0;
    min-height: 100vh;
    width: 100%;
    
    &.modal-open {
      overflow: hidden;
    }
  }
  
  a {
    color: inherit;
    text-decoration: none;
  }

  * {
    box-sizing: border-box;
  }
`;

export default GlobalStyle;
