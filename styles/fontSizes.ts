type FontSizes = {
  md: string;
  lg: string;
  xl: string;
  xl2: string;
};

const fontSizes: FontSizes = {
  md: "1rem",
  /** 18px */
  lg: "1.125rem",
  /** 20px */
  xl: "1.25rem",
  /** 40px */
  xl2: "2.5rem",
};

export { fontSizes };
export type { FontSizes };
