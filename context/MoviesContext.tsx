import { createContext, useState } from "react";

const MoviesContext = createContext({});

const MoviesProvider = ({ children }: any) => {
  const [movieDetails, setMovieDetails] = useState({});
  const [movieId, setMovieId] = useState(null);
  const [movies, setMovies] = useState([]);
  const [page, setPage] = useState(1);
  const [results, setResults] = useState(0);
  const [search, setSearch] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [totalPages, setTotalPages] = useState(null);

  return (
    <MoviesContext.Provider
      value={{
        movieDetails,
        setMovieDetails,
        movieId,
        setMovieId,
        movies,
        setMovies,
        page,
        setPage,
        results,
        setResults,
        search,
        setSearch,
        showModal,
        setShowModal,
        totalPages,
        setTotalPages,
      }}
    >
      {children}
    </MoviesContext.Provider>
  );
};

export { MoviesContext, MoviesProvider };
