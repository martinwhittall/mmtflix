import { FC } from "react";
import Image from "next/image";
import Link from "next/link";
import styled from "styled-components";
import Search from "./SearchForm";

const Header: FC = () => {
  return (
    <StyledHeader>
      <Link href="/" passHref>
        <LogoLink>
          <Image
            src="/images/logo.svg"
            alt="MMTFlix Logo"
            height={48}
            width={296}
          />
          <h1 aria-label="Welcome to MMTFlix" />
        </LogoLink>
      </Link>

      <StyledSearch />
    </StyledHeader>
  );
};

const StyledHeader = styled.header`
  padding: ${({ theme }) => theme.spacing.xl2} 0
    ${({ theme }) => theme.spacing.lg};
  text-align: center;

  @media only screen and (min-width: ${({ theme }) => theme.breakpoints.lg}) {
    align-items: center;
    display: flex;
    justify-content: space-between;
    padding: ${({ theme }) => theme.spacing.xl4} 0
      ${({ theme }) => theme.spacing.xl3};
  }
`;

const LogoLink = styled.a`
  @media only screen and (min-width: ${({ theme }) => theme.breakpoints.lg}) {
    margin: 0 0 0 ${({ theme }) => theme.spacing.xl};
  }

  &:focus {
    outline: 1px dotted ${({ theme }) => theme.borderColors.borderColor1};
  }
`;

const StyledSearch = styled(Search)`
  margin: ${({ theme }) => theme.spacing.lg} auto 0;
  max-width: 500px;

  @media only screen and (min-width: ${({ theme }) => theme.breakpoints.lg}) {
    margin: 0 0 0 ${({ theme }) => theme.spacing.xl3};
  }
`;

export default Header;
