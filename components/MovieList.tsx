import React, { FC, useContext, useEffect } from "react";
import { MoviesContext } from "../context/MoviesContext";
import styled from "styled-components";
import MovieItem from "./MovieItem";
import Modal from "./Modal";
import { Movie } from "../types/Movie";
import { Context } from "../types/Context";

const MovieList: FC = () => {
  const { movies, setMovies, page, search, setResults, setTotalPages } =
    useContext(MoviesContext) as Context;

  useEffect(() => {
    fetch(`/api/movies?search=${search}&page=${page}`)
      .then((response) => response.json())
      .then((data) => {
        setMovies(data.movies);
        setResults(data.totalResults);
        setTotalPages(data.totalPages);
      });
  }, [page, search, setMovies, setResults, setTotalPages]);

  return (
    <>
      <Modal />

      {movies && (
        <StyledMovieList>
          {movies.map((movie: Movie, index: number) => {
            return <StyledMovieItem key={index} {...movie} />;
          })}
        </StyledMovieList>
      )}
    </>
  );
};

const StyledMovieList = styled.ul`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin: -${({ theme }) => theme.spacing.sm} -${({ theme }) =>
      theme.spacing.xs3};
`;

const StyledMovieItem = styled(MovieItem)`
  max-width: 264px;
  margin: ${({ theme }) => theme.spacing.sm} ${({ theme }) =>
  theme.spacing.xs3};
\` ;
`;

export default MovieList;
