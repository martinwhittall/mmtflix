import React, { FC, useContext } from "react";
import Image from "next/image";
import styled from "styled-components";
import { Movie } from "../types/Movie";
import { MoviesContext } from "../context/MoviesContext";
import { Context } from "../types/Context";

const MovieItem: FC<Movie> = ({ className, id, image, title, year }) => {
  const { setMovieId, showModal, setShowModal } = useContext(
    MoviesContext
  ) as Context;

  const handleKeyPress = (event: React.KeyboardEvent<HTMLElement>) => {
    if (event.key === "Enter") {
      event.preventDefault();
      openModal();
    }
  };

  const openModal = () => {
    setMovieId(id);
    setShowModal(!showModal);
  };

  return (
    <StyledMovie
      className={`movie ${className}`}
      onClick={() => openModal()}
      onKeyPress={(event) => handleKeyPress(event)}
      tabIndex={0}
    >
      <Image
        alt={image ? `${title} poster` : "/images/placeholder.svg"}
        className="movie__image"
        height={396}
        width={264}
        src={image ? image : "/images/placeholder.svg"}
      />

      <div className="movie__contents">
        <p className="movie__title">{title}</p>
        <p className="movie__release-year">{year}</p>
      </div>
    </StyledMovie>
  );
};

const StyledMovie = styled.li`
  cursor: pointer;

  &:focus {
    outline: 1px dotted ${({ theme }) => theme.borderColors.borderColor1};
  }

  .movie {
    &__title,
    &__release-year {
      font-weight: 700;
    }
  }
`;

export default MovieItem;
