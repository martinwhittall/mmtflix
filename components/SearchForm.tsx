import React, { FC, useContext, useRef } from "react";
import { MoviesContext } from "../context/MoviesContext";
import styled from "styled-components";
import Image from "next/image";
import { Context } from "../types/Context";

// @ts-ignore
const SearchForm: FC = ({ className }) => {
  const { search, setSearch } = useContext(MoviesContext) as Context;

  const searchInput = useRef<HTMLInputElement | null>(null);

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      event.preventDefault();
      handleSearch();
    }
  };

  const handleSearch = () => {
    // @ts-ignore
    setSearch(searchInput.current.value);
  };

  return (
    <StyledSearch className={`search ${className}`}>
      <div className="search__field">
        <input
          aria-label="Search movie"
          className="search__input"
          data-testid="search-input"
          defaultValue={search}
          id="search"
          onKeyPress={(event) => handleKeyPress(event)}
          placeholder="Search here..."
          name="search"
          ref={searchInput}
          type="text"
        />

        <button
          aria-label="Search"
          className="search__btn"
          type="button"
          onClick={handleSearch}
        >
          <Image src="/images/search.svg" alt="Search" height={27} width={25} />
        </button>
      </div>
    </StyledSearch>
  );
};

const StyledSearch = styled.form`
  width: 100%;

  .search {
    &__field {
      position: relative;
      width: 100%;
    }

    &__input {
      background: transparent;
      border: 2px solid ${({ theme }) => theme.borderColors.borderColor1};
      color: ${({ theme }) => theme.fontColors.fontColor2};
      font-size: ${({ theme }) => theme.fontSizes.lg};
      font-weight: 700;
      height: 50px;
      outline: none;
      padding: 0 ${({ theme }) => theme.spacing.sm};
      width: 100%;

      &::placeholder {
        color: ${({ theme }) => theme.fontColors.fontColor2};
      }

      &:focus {
        outline: 1px dotted ${({ theme }) => theme.borderColors.borderColor1};
      }
    }

    &__btn {
      appearance: none;
      background: transparent;
      border: none;
      bottom: 0;
      cursor: pointer;
      outline: none;
      padding: ${({ theme }) => theme.spacing.sm};
      position: absolute;
      right: 0;
      top: 0;

      &:focus {
        outline: 1px dotted ${({ theme }) => theme.borderColors.borderColor1};
      }
    }
  }
`;

export default SearchForm;
