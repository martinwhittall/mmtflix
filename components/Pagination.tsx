import { FC, useContext, useEffect, useState } from "react";
import Image from "next/image";
import styled from "styled-components";
import { MoviesContext } from "../context/MoviesContext";
import { Context } from "../types/Context";

const Pagination: FC = () => {
  const { movies, page, results, setPage, totalPages } = useContext(
    MoviesContext
  ) as Context;

  const isPreviousDisabled = page === 1;
  const isNextDisabled = page === totalPages;

  return (
    <>
      {movies.length > 0 && (
        <StyledPagination className="pagination">
          <p className="pagination__results">{results} Results found</p>

          <div className="pagination__actions">
            <p className="pagination__page-count">
              Page {page} of {totalPages}
            </p>

            <div className="pagination__btns">
              <button
                aria-disabled={isPreviousDisabled}
                aria-label="Previous"
                className="pagination__btn"
                disabled={isPreviousDisabled}
                onClick={() => setPage(page - 1)}
                type="button"
              >
                <Image
                  alt="Back arrow icon"
                  height={15}
                  src="/images/back-arrow.svg"
                  width={22}
                />
              </button>

              <button
                aria-disabled={isNextDisabled}
                aria-label="Next"
                className="pagination__btn"
                disabled={isNextDisabled}
                onClick={() => setPage(page + 1)}
                type="button"
              >
                <Image
                  alt="Forward arrow icon"
                  height={15}
                  src="/images/forward-arrow.svg"
                  width={22}
                />
              </button>
            </div>
          </div>
        </StyledPagination>
      )}
    </>
  );
};

const StyledPagination = styled.div`
  margin-bottom: ${({ theme }) => theme.spacing.lg};
  text-align: center;

  @media only screen and (min-width: ${({ theme }) => theme.breakpoints.lg}) {
    align-items: center;
    display: flex;
    justify-content: space-between;
  }

  .pagination {
    &__actions {
      @media only screen and (max-width: ${({ theme }) =>
          theme.breakpoints.lg}) {
        background: ${({ theme }) => theme.backgroundColors.backgroundColor1};
        bottom: 0;
        left: 0;
        padding: ${({ theme }) => theme.spacing.xs}
          ${({ theme }) => theme.spacing.lg} ${({ theme }) => theme.spacing.lg};
        position: fixed;
        right: 0;
        z-index: 10;
      }

      @media only screen and (min-width: ${({ theme }) =>
          theme.breakpoints.lg}) {
        align-items: center;
        display: flex;
      }
    }

    &__results,
    &__page-count {
      font-weight: 700;
      font-size: ${({ theme }) => theme.fontSizes.lg};
      line-height: 24px;
    }

    &__page-count {
      margin-bottom: ${({ theme }) => theme.spacing.xs};

      @media only screen and (min-width: ${({ theme }) =>
          theme.breakpoints.lg}) {
        margin: 0 ${({ theme }) => theme.spacing.lg} 0 0;
      }
    }

    &__btn {
      appearance: none;
      background: transparent;
      border: 2px solid ${({ theme }) => theme.borderColors.borderColor1};
      cursor: pointer;
      color: ${({ theme }) => theme.fontColors.fontColor2};
      height: 44px;
      width: 44px;

      &:not(:first-child) {
        margin-left: ${({ theme }) => theme.spacing.lg};
      }

      :disabled {
        background-color: ${({ theme }) =>
          theme.backgroundColors.backgroundColor4};
        border: 1px solid ${({ theme }) => theme.borderColors.borderColor2};
        color: ${({ theme }) => theme.fontColors.fontColor1};
        cursor: not-allowed;
      }
    }
  }
`;

export default Pagination;
