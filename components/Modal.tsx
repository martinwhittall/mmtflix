import React, { FC, useContext, useEffect } from "react";
import styled from "styled-components";
import { MoviesContext } from "../context/MoviesContext";
import { Context } from "../types/Context";
import { MovieDetails } from "../types/MoviesDetails";
import Image from "next/image";

const Modal: FC = () => {
  const { movieDetails, setMovieDetails, movieId, showModal, setShowModal } =
    useContext(MoviesContext) as Context;

  useEffect(() => {
    fetch(`/api/movies/details?id=${movieId}`)
      .then((response) => response.json())
      .then((data) => {
        setMovieDetails(data);
      });
  }, [movieId, setMovieDetails]);

  const { title, cast, plot, image, genres, year, directors } =
    movieDetails as MovieDetails;

  if (typeof window !== "undefined") {
    showModal
      ? document.body.classList.add("modal-open")
      : document.body.classList.remove("modal-open");
  }

  return (
    <>
      {showModal && (
        <StyledModal className="modal">
          <div className="modal__wrapper">
            <div className="modal__container">
              <div className="modal__body" role="dialog" aria-modal="true">
                <button
                  aria-label="Close"
                  className="modal__close"
                  onClick={() => setShowModal(false)}
                >
                  <Image
                    alt="Close icon"
                    height={22}
                    src="/images/close.svg"
                    width={22}
                  />
                </button>

                <div className="modal__image-container">
                  <Image
                    alt={image ? `${title} poster` : "/images/placeholder.svg"}
                    className="modal__image"
                    height={396}
                    width={264}
                    src={image ? image : "/images/placeholder.svg"}
                  />
                </div>

                <div className="modal__contents">
                  <div className="modal__header">
                    <p className="modal__title">{title}</p>
                    <p className="modal__release-year">({year})</p>
                  </div>

                  <div className="modal__info">
                    {directors && (
                      <p className="modal__directors | directors">
                        <span className="directors__title">Director:</span>
                        {directors
                          .map((director: string) => director)
                          .join(", ")}
                      </p>
                    )}

                    {cast && (
                      <p className="modal__cast | cast">
                        <span className="cast__title">Cast:</span>
                        {cast.map((cast: string) => cast).join(", ")}
                      </p>
                    )}

                    {genres && (
                      <p className="modal__genres | genres">
                        <span className="genres__title">Genres:</span>
                        {genres.map((genre: string) => genre).join(", ")}
                      </p>
                    )}
                  </div>

                  {plot && <p className="modal__overview">{plot}</p>}
                </div>
              </div>
            </div>
          </div>
        </StyledModal>
      )}
    </>
  );
};

export default Modal;

const StyledModal = styled.div`
  align-items: flex-start;
  background-color: rgba(0, 0, 0, 0.95);
  bottom: 0;
  display: flex;
  height: 100%;
  overflow-x: hidden;
  justify-content: flex-start;
  left: 0;
  position: fixed;
  width: 100%;
  z-index: 100;

  @media only screen and (min-width: ${({ theme }) => theme.breakpoints.md}) {
    align-items: center;
  }

  .modal {
    &__wrapper {
      max-height: 100%;
      min-width: 100%;
    }

    &__container {
      overflow-y: auto;
      overflow-x: hidden;

      @media only screen and (min-width: ${({ theme }) =>
          theme.breakpoints.md}) {
        margin: ${({ theme }) => theme.spacing.xl3};
      }
    }

    &__body {
      background: ${({ theme }) => theme.backgroundColors.backgroundColor2};
      padding: ${({ theme }) => theme.spacing.xl3}
        ${({ theme }) => theme.spacing.lg};
      margin: 0 auto;
      max-width: 920px;
      min-height: 100vh;
      justify-content: flex-end;
      position: relative;
      width: 100%;

      @media only screen and (min-width: ${({ theme }) =>
          theme.breakpoints.md}) {
        display: flex;
        min-height: 100%;
      }

      @media only screen and (min-width: ${({ theme }) =>
          theme.breakpoints.lg}) {
        padding: ${({ theme }) => theme.spacing.xl3};
      }
    }

    &__close {
      appearance: none;
      background: ${({ theme }) => theme.backgroundColors.backgroundColor1};
      border: none;
      border-radius: 50%;
      cursor: pointer;
      height: 30px;
      outline: none;
      position: absolute;
      right: ${({ theme }) => theme.spacing.xs};
      top: ${({ theme }) => theme.spacing.xs};
      width: 30px;

      &:hover {
        border: 2px solid ${({ theme }) => theme.borderColors.borderColor1};
      }

      &:focus {
        outline: 1px dotted ${({ theme }) => theme.borderColors.borderColor1};
      }
    }

    &__image-container {
      margin-bottom: ${({ theme }) => theme.spacing.xl};
      flex: 0 0 264px;
      text-align: center;

      @media only screen and (min-width: ${({ theme }) =>
          theme.breakpoints.md}) {
        margin: 0 ${({ theme }) => theme.spacing.xl3} 0 0;
      }
    }

    &__title {
      display: inline-block;
      font-size: ${({ theme }) => theme.fontSizes.xl2};
      line-height: 48px;
      margin-right: ${({ theme }) => theme.spacing.xs};
    }

    &__release-year {
      display: inline-block;
      font-size: ${({ theme }) => theme.fontSizes.xl};
    }

    &__info {
      font-size: ${({ theme }) => theme.fontSizes.lg};

      .directors__title,
      .cast__title,
      .genres__title {
        font-weight: 400;
        margin-right: ${({ theme }) => theme.spacing.xs};
      }

      > :not(:first-child) {
        margin-top: ${({ theme }) => theme.spacing.xs2};
      }
    }

    &__info,
    &__overview {
      margin-top: ${({ theme }) => theme.spacing.sm};

      @media only screen and (min-width: ${({ theme }) =>
          theme.breakpoints.lg}) {
        margin-top: ${({ theme }) => theme.spacing.xl2};
      }
    }
  }
`;
